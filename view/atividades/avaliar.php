<?php
    include_once '../compartilhado/sharedLogado.php';
    include_once '../../controller/atividade/listar.php';

    $get = $_GET;
    
    $idAtividade = $get['IdAtividade'];
    $atividade = BuscaAtividadePorId($idAtividade);
    
    if(VerificaUsuarioCriadorAtividade($idAtividade) == 0) echo '<script>window.location.href="'.BASE_URL.'view/compartilhado/acessonegado.php"</script>';
?>

<body>
    <div class="container mt-5 mb-5">
    <br/>
    
        <div class="card border-primary">
            <div class="card-header text-center text-white bg-primary">Atividade</div>
            <div class="card-body">
                <form action="../../controller/atividade/avaliar.php" method="POST">
                <input type="hidden" id="IdCriador" name="IdCriador" value="<?php echo $atividade->GetIdCriador(); ?>">
                <input type="hidden" id="Ativo" name="Ativo" value="<?php echo $atividade->GetAtivo(); ?>">
                <input type="hidden" id="IdUsuarioAtribuido" name="IdUsuarioAtribuido" value="<?php echo $atividade->GetIdUsuarioAtribuido(); ?>">
                <input type="hidden" id="DataCriacao" name="DataCriacao" value="<?php echo $atividade->GetDataCriacao(); ?>">
                <input type="hidden" id="IdGrupo" name="IdGrupo" value="<?php echo $atividade->GetIdGrupo(); ?>">
                <input type="hidden" id="Atividade" name="Atividade" value="<?php echo $atividade->GetAtividade(); ?>">
                
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="IdAtividade">Atividade</label>
                                <select id="IdAtividade" name="IdAtividade" class="form-control">
                                    <?php echo '<option value="'.$atividade->GetIdAtividade().'">'.$atividade->GetAtividade().'</option>';?>
                                </select>
                            </div>
                        </div>
                    </div>                   
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="Descricao">Descrição da Atividade</label>
                                <textarea readonly class="form-control" id="Descricao" name="Descricao" rows="3"><?php echo $atividade->GetDescricao()?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="IdPrioridade">Prioridade</label>
                                <select id="IdPrioridade" name="IdPrioridade" class="form-control">
                                    <?php echo '<option value="'.$atividade->GetIdPrioridade().'">'.$atividade->GetNomePrioridade().'</option>';?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="IdStatus">Status</label>
                                <select id="IdStatus" name="IdStatus" class="form-control">
                                <?php echo '<option value="'.$atividade->GetIdStatus().'">'.$atividade->GetNomeStatus().'</option>';?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="IdAvaliacao">Avaliação</label>
                                <select id="IdAvaliacao" name="IdAvaliacao" class="form-control">
                                    <?php 
                                        $idAvaliacao = $atividade->GetIdAvaliacao();
                                        if($idAvaliacao == null)
                                        {
                                            echo '<option value="1">Ótimo</option>';
                                            echo '<option value="2">Bom</option>';
                                            echo '<option value="3">Regular</option>';
                                            echo '<option value="4">Ruim</option>';
                                            echo '<option value="5">Péssimo</option>';
                                        }
                                        else
                                        {
                                            if($idAvaliacao==1)
                                            {
                                                echo '<option value="1" selected>Ótimo</option>';
                                                echo '<option value="2">Bom</option>';
                                                echo '<option value="3">Regular</option>';
                                                echo '<option value="4">Ruim</option>';
                                                echo '<option value="5">Péssimo</option>';
                                            }
                                            else if($idAvaliacao==2)
                                            {
                                                echo '<option value="1">Ótimo</option>';
                                                echo '<option value="2" selected>Bom</option>';
                                                echo '<option value="3">Regular</option>';
                                                echo '<option value="4">Ruim</option>';
                                                echo '<option value="5">Péssimo</option>';
                                            }
                                            else if($idAvaliacao==3)
                                            {
                                                echo '<option value="1">Ótimo</option>';
                                                echo '<option value="2">Bom</option>';
                                                echo '<option value="3" selected>Regular</option>';
                                                echo '<option value="4">Ruim</option>';
                                                echo '<option value="5">Péssimo</option>';
                                            }
                                            else if($idAvaliacao==4)
                                            {
                                                echo '<option value="1">Ótimo</option>';
                                                echo '<option value="2">Bom</option>';
                                                echo '<option value="3">Regular</option>';
                                                echo '<option value="4" selected>Ruim</option>';
                                                echo '<option value="5">Péssimo</option>';
                                            }
                                            else if($idAvaliacao==5)
                                            {
                                                echo '<option value="1">Ótimo</option>';
                                                echo '<option value="2">Bom</option>';
                                                echo '<option value="3">Regular</option>';
                                                echo '<option value="4">Ruim</option>';
                                                echo '<option value="5" selected>Péssimo</option>';
                                            }
                                        }
                                        
                                    ;?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                </form>
            </div>
        </div>
    </div>
</body>