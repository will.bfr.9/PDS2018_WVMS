<?php
    include_once '../compartilhado/sharedLogado.php';
    include_once '../../controller/prioridade/listar.php';
    include_once '../../controller/grupo/listar.php';

    $get = $_GET;
    
    $idGrupo = $get['IdGrupo'];
    $nomeGrupo = BuscarGrupoId($idGrupo)->GetGrupo();

    echo $nomeGrupo;
    
    if(PodeAtribuirAtividade($idGrupo) == 0) echo '<script>window.location.href="'.BASE_URL.'view/compartilhado/acessonegado.php"</script>';
?>

<body>
    <div class="container mt-5 mb-5">
    <br/>
    
        <div class="card border-primary">
            <div class="card-header text-center text-white bg-primary">Cadastro de Atividade</div>
            <div class="card-body">
                <form action="../../controller/atividade/inserir.php" method="POST">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="Atividade">Nome da Atividade</label>
                                <input type="text" class="form-control" id="Atividade" name="Atividade">
                            </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="IdGrupo">Grupo</label>
                                    <select id="IdGrupo" name="IdGrupo" class="form-control">
                                        <?php echo '<option value="'.$idGrupo.'">'.$nomeGrupo.'</option>';?>
                                    </select>
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="IdUsuarioAtribuido">Usuário Responsável</label>
                                <select class="form-control" id="IdUsuarioAtribuido" name="IdUsuarioAtribuido">
                                    <?php 
                                        $lista = RetornaMembrosGrupo($idGrupo);

                                        foreach($lista as $row)
                                        {
                                            echo '<option value="'.$row->GetIdUsuario().'">'.$row->GetNomeUsuario().'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="Atividade">Prioridade</label>
                                <select class="form-control" id="IdPrioridade" name="IdPrioridade">
                                    <?php 
                                        $lista = ListarPrioridade();

                                        foreach($lista as $row)
                                        {
                                            echo '<option value="'.$row->GetIdPrioridade().'">'.$row->GetPrioridade().'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="Descricao">Descrição da Atividade</label>
                                <textarea class="form-control" id="Descricao" name="Descricao" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</body>