<?php
    include_once '../compartilhado/sharedLogado.php';
?>

<body>
    <div class="container mt-5 mb-5">
    <br/>
        <?php 
            $lista = RetornaAtividadesCriadasUsuario();
            if(is_array($lista))
            {
                echo'<table class="table table-striped table-bordered">';
                    echo'<thead>';
                        echo'<tr class="bg-primary">';
                            echo'<th scope="col">Grupo</th>';
                            echo'<th scope="col">Atividade</th>';
                            echo'<th scope="col">Usuário Atribuído</th>';
                            echo'<th scope="col">Prioridade</th>';
                            echo'<th scope="col">Status</th>';
                            echo'<th scope="col">Criada em</th>';
                        echo'</tr>';
                    echo'</thead>';
                    echo'<tbody>';
                
                    foreach($lista as $item)
                    {
                        echo '<tr>';
                            echo '<td>'.$item->GetNomeGrupo().'</td>';
                            echo '<td>'.$item->GetAtividade().'</td>';
                            echo '<td>'.$item->GetNomeAtribuido().'</td>';
                            echo '<td>'.$item->GetNomePrioridade().'</td>';
                            echo '<td>'.$item->GetNomeStatus().'</td>';
                            echo '<td>'.date('d-m-Y', strtotime($item->GetDataCriacao())).'</td>';

                        echo '</tr>';

                    }
                    echo '</tbody>';
                echo '</table>';
            }
            else
            {
                echo '<div class="alert alert-success" role="alert">';
                    echo '<h4 class="alert-heading">Parabéns!</h4>';
                    echo '<p>Você não possui nenhuma atividade em aberto.</p>';
                    echo '<hr>';
                    echo '<p class="mb-0">Continue assim realizando suas tarefas em dia.</p>';
                echo '</div>';
            }
            ?>  
    </div>
</body>