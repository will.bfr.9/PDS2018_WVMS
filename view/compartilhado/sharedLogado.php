<?php 
    include_once '../../config/config.php';
    include_once '../../controller/UsuarioGrupo/Listar.php';
    include_once '../../controller/atividade/Listar.php';
    header('Content-type: text/html; charset=UTF-8');

    function AuxiliaSimNao($entrada)
    {
        if($entrada == true)
        {
            echo '<option value="1" selected>Sim</option>';
            echo '<option value="0">Não</option>';
        }
        else
        {
            echo '<option value="1">Sim</option>';
            echo '<option value="0" selected>Não</option>';
        }
    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <link rel="stylesheet" type="text/css" href="<?php echo CSS_URL. 'layout.css';?>">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
            <a class="navbar-brand" href="<?php echo BASE_URL . 'view/atividades/listar.php';?>">Home</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Grupos
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="../grupo/novo.php">Criar Grupo</a>
                            <a class="dropdown-item" href="../grupousuario/listar.php">Meus Grupos
                                <span class="badge badge-pill badge-primary"><?php echo RetornaQtd();?></span></a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Atividades
                            <?php $retornoQtdAtividade = RetornaQtdAtividadesAbertas();
                            echo '<span class="badge badge-pill badge-';
                            echo $retornoQtdAtividade == 0 ? 'success' : 'danger';
                            echo '">'.$retornoQtdAtividade.'</span>';
                            ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <?php echo '<a class="dropdown-item" href="../atividades/novo.php?IdGrupo='.AtividadeRapida().'">Criar Atividade Rápida</a>'; ?>
                            <a class="dropdown-item" href="../atividades/listar.php">Minhas Atividades</a>
                            <a class="dropdown-item" href="../atividades/atividadescriadaspormim.php">Atividades Criadas por Mim</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Avaliação
                            <?php $retornoQtdAtividade = RetornaQtdAtividadesParaAvaliar();
                            echo '<span class="badge badge-pill badge-';
                            echo $retornoQtdAtividade == 0 ? 'success' : 'warning';
                            echo '">'.$retornoQtdAtividade.'</span>';
                            ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="../atividades/listaravaliar.php">Avaliar Atividades</a>
                            <a class="dropdown-item" href="../avaliacao/minhasavaliacoes.php">Relatório de Avaliações</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Perfil
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="../../controller/usuario/logout.php">Sair</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <?php if(is_array($_SESSION['logado']) == false) echo '<script>window.location.href="'.BASE_URL.'view/compartilhado/acessonegado.php"</script>'; ?>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="<?php echo JS_URL. 'sweetalert2.all.min.js'; ?>"></script>
    </body>
    <nav class="navbar text-white bg-primary fixed-bottom" style="height: 7%">
    <div class="pt-4">
      <p>Desenvolvido por: William Vinícius Mendes Silveira - 002201701161</p>
    </div> 
  </nav>
</html>