<?php
    include_once '../compartilhado/shared.php';
    include_once '../../model/modelBase.php';
?>

<body>
    <div class="container">
    <br/>
        <div class="row">
            <div class="col-sm-9">
                <div class="card text-white border-primary bg-white text-primary mt-5">
                    <div class="card-header">SEJA BEM VINDO AO M.B.A.</div>
                    <div class="card-body">
                        <h5 class="card-title">MBA - Meu Bloquinho de Atividades</h5>
                        <p class="card-text" style="text-indent: 1em">O MBA é um sistema que para cadastro e compartilhamento de atividades do dia-a-dia. Com ele você pode criar um grupo e adicionar seus amigos e familiares para atribuir as atividades diárias, buscando sempre ajudar em um gerenciamento e melhorar as tarefas.</p>
                        <p class="card-text">Para fazer o login no MBA, entre com seu e-mail e senha no painel ao lado.</p>
                        <p class="card-text">Caso você ainda não possui cadastro, <a href="<?php echo BASE_URL .'view/home/cadastro.php'; ?>"> clique aqui </a> para fazer o cadastro.</p>
                        <br/>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card border-primary mt-5" id="CardLogin">
                    <div class="card-header text-center text-white bg-primary">
                        Login
                    </div>
                    <div class="card-body">
                        <form action="../../controller/usuario/login.php" method="POST">
                            <div class="form-group">
                                <label for="Email">Email</label>
                                <input type="email" class="form-control" id="Email" name="Email">
                            </div>
                            <div class="form-group">
                                <label for="Senha">Senha</label>
                                <input type="password" class="form-control" id="Senha"  name="Senha" >
                            </div>
                            <button type="submit" class="btn btn-primary">Entrar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
