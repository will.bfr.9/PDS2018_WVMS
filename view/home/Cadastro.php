<?php
    include_once '../compartilhado/shared.php';
    include_once '../../model/modelBase.php';
?>

<body>
    <div class="container mt-5 mb-5">
    <br/>
        <div class="card border-primary">
            <div class="card-header text-center text-white bg-primary">Cadastro de Usuário</div>
            <div class="card-body">
                <form action="../../controller/usuario/inserir.php" method="POST">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="NomeUsuario">Nome</label>
                                <input type="text" class="form-control" id="NomeUsuario" name="NomeUsuario">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="Email">Email</label>
                                <input type="Email" class="form-control" id="Email" name="Email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="Senha">Senha</label>
                                <input type="password" class="form-control" id="Senha" name="Senha">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</body>