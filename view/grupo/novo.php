<?php
    include_once '../compartilhado/sharedLogado.php';
    
?>

<body>
    <div class="container mt-5 mb-5">
    <br/>
        <div class="card border-primary">
            <div class="card-header text-center text-white bg-primary">Cadastro de Grupo</div>
            <div class="card-body">
                <form action="../../controller/grupo/inserir.php" method="POST">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="Grupo">Nome do Grupo</label>
                                <input type="text" class="form-control" id="Grupo" name="Grupo">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="Descricao">Descrição do Grupo</label>
                                <textarea class="form-control" id="Descricao" name="Descricao" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</body>