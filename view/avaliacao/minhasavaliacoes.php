<?php
    include_once '../compartilhado/sharedLogado.php';
    include_once '../../controller/Avaliacao/minhasavaliacoes.php';
?>

<body>
    <div class="container mt-5 mb-5">
        <br/>
        <?php 
            $quantidadeAtividadesTotal = AtividadesTotal(); 
            $quantidadeAtividadesFeitas = AtividadesFeitas(); 
            $quantidadeAtividadesAvaliadas = AtividadesAvaliadas();
            $otimo = AtividadesPorAvaliacao(1);
            $bom = AtividadesPorAvaliacao(2);
            $regular = AtividadesPorAvaliacao(3);
            $ruim = AtividadesPorAvaliacao(4);
            $pessimo = AtividadesPorAvaliacao(5);

			if($quantidadeAtividadesTotal != 0)
			{
				echo '<p>Atividades Feitas: '.$quantidadeAtividadesFeitas.'/'.$quantidadeAtividadesTotal .'</p>';
				echo '<div class="progress">';
				echo '<div class="progress-bar" role="progressbar" style="width: '.(100*($quantidadeAtividadesFeitas/$quantidadeAtividadesTotal)).'%" aria-valuenow="'.$quantidadeAtividadesTotal.'" aria-valuemin="0" aria-valuemax="'.$quantidadeAtividadesTotal.'"></div>';
				echo '</div>';
				echo '<br/>';
			}
			else
			{
				echo '<p>Você ainda não tem Atividades Atribuidas</p>';
			}
            
			if($quantidadeAtividadesFeitas != 0)
			{
				echo '<p>Atividades Avaliadas: '.$quantidadeAtividadesAvaliadas.'/'.$quantidadeAtividadesFeitas .'</p>';
				echo '<div class="progress">';
				echo '<div class="progress-bar" role="progressbar" style="width: '.(100*($quantidadeAtividadesAvaliadas/$quantidadeAtividadesFeitas)).'%" aria-valuenow="'.$quantidadeAtividadesTotal.'" aria-valuemin="0" aria-valuemax="'.$quantidadeAtividadesTotal.'"></div>';
				echo '</div>';
				echo '<br/>';
			}
            else
			{
				echo '<p>Você ainda não tem Atividades Concluídas</p>';
			}

			if($quantidadeAtividadesAvaliadas != 0)
			{
				//Otimo
				echo '<p>Atividades Avaliada como Ótimo: '.$otimo.'/'.$quantidadeAtividadesAvaliadas .'</p>';
				echo '<div class="progress">';
				echo '<div class="progress-bar bg-success" role="progressbar" style="width: '.(100*($otimo/$quantidadeAtividadesAvaliadas)).'%" aria-valuenow="'.$quantidadeAtividadesTotal.'" aria-valuemin="0" aria-valuemax="'.$quantidadeAtividadesTotal.'"></div>';
				echo '</div>';
				echo '<br/>';

				//Bom
				echo '<p>Atividades Avaliada como Bom: '.$bom.'/'.$quantidadeAtividadesAvaliadas .'</p>';
				echo '<div class="progress">';
				echo '<div class="progress-bar bg-info" role="progressbar" style="width: '.(100*($bom/$quantidadeAtividadesAvaliadas)).'%" aria-valuenow="'.$quantidadeAtividadesTotal.'" aria-valuemin="0" aria-valuemax="'.$quantidadeAtividadesTotal.'"></div>';
				echo '</div>';
				echo '<br/>';

				//Regular
				echo '<p>Atividades Avaliada como Regular: '.$regular.'/'.$quantidadeAtividadesAvaliadas .'</p>';
				echo '<div class="progress">';
				echo '<div class="progress-bar bg-warning" role="progressbar" style="width: '.(100*($regular/$quantidadeAtividadesAvaliadas)).'%" aria-valuenow="'.$quantidadeAtividadesTotal.'" aria-valuemin="0" aria-valuemax="'.$quantidadeAtividadesTotal.'"></div>';
				echo '</div>';
				echo '<br/>';

				//Ruim
				echo '<p>Atividades Avaliada como Ruim: '.$ruim.'/'.$quantidadeAtividadesAvaliadas .'</p>';
				echo '<div class="progress">';
				echo '<div class="progress-bar bg-secondary" role="progressbar" style="width: '.(100*($ruim/$quantidadeAtividadesAvaliadas)).'%" aria-valuenow="'.$quantidadeAtividadesTotal.'" aria-valuemin="0" aria-valuemax="'.$quantidadeAtividadesTotal.'"></div>';
				echo '</div>';
				echo '<br/>';

				//Pessimo
				echo '<p>Atividades Avaliada como Péssimo: '.$pessimo.'/'.$quantidadeAtividadesAvaliadas .'</p>';
				echo '<div class="progress">';
				echo '<div class="progress-bar bg-danger" role="progressbar" style="width: '.(100*($pessimo/$quantidadeAtividadesAvaliadas)).'%" aria-valuenow="'.$quantidadeAtividadesTotal.'" aria-valuemin="0" aria-valuemax="'.$quantidadeAtividadesTotal.'"></div>';
				echo '</div>';
				echo '<br/>';
			}
			else
			{
				echo '<p>Você ainda não tem Atividades Avaliadas</p>';
			}
            

        ?>
    </div>
</body>