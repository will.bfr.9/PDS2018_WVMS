<?php
    include_once '../compartilhado/sharedLogado.php';
    include_once '../../controller/prioridade/listar.php';
    include_once '../../controller/grupo/listar.php';

    $get = $_GET;
    
    $idUsuarioGrupo = $get['IdUsuarioGrupo'];
    $usuarioGrupo = RetornaUsuarioGrupo($idUsuarioGrupo);
    if(AdministraGrupo($usuarioGrupo->GetIdGrupo()) == 0) echo '<script>window.location.href="'.BASE_URL.'view/compartilhado/acessonegado.php"</script>';
    $criador = BuscarGrupoId($usuarioGrupo->GetIdGrupo())->GetIdUsuario();
    if($criador == $usuarioGrupo->GetIdUsuario())
    {
        echo '<script>swal("Você não pode alterar as permissões do criador do Grupo. Mesmo que você seja o Criador.", "Ocorreu um erro!", "error").then((value) => {
            window.location.href="'.BASE_URL.'view/compartilhado/acessonegado.php";
        }); </script>';
    } 
    
?>

<body>
    <div class="container mt-5 mb-5">
    <br/>
    
        <div class="card border-primary">
            <div class="card-header text-center text-white bg-primary">Permissões</div>
            <div class="card-body">
                <form action="../../controller/usuariogrupo/atualizar.php" method="POST">
                    <input type="hidden" id="IdUsuarioGrupo" name="IdUsuarioGrupo" value="<?php echo $usuarioGrupo->GetIdUsuarioGrupo(); ?>">
                    <div class="row">
                    <div class="col-sm-6">
                            <div class="form-group">
                                <label for="IdUsuario">Usuário</label>
                                <select class="form-control" id="IdUsuario" name="IdUsuario">
                                    <?php 
                                        echo '<option value="'.$usuarioGrupo->GetIdUsuario().'">'.$usuarioGrupo->GetNomeUsuario().'</option>';
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="IdGrupo">Grupo</label>
                                    <select id="IdGrupo" name="IdGrupo" class="form-control">
                                        <?php echo '<option value="'.$usuarioGrupo->GetIdGrupo().'">'.$usuarioGrupo->GetNomeGrupo().'</option>';?>
                                    </select>
                                </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-sm-3">
                            <div class="form-group">
                                <label for="Atividade">Pode Compartilhar</label>
                                <select class="form-control" id="PodeCompartilhar" name="PodeCompartilhar">
                                    <?php AuxiliaSimNao($usuarioGrupo->GetPodeCompartilhar()); ?>                        
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="Atividade">Atribui Atividade</label>
                                <select class="form-control" id="AtribuiAtividade" name="AtribuiAtividade">
                                    <?php AuxiliaSimNao($usuarioGrupo->GetAtribuiAtividade()); ?>                        
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="Atividade">Administra Grupo</label>
                                <select class="form-control" id="AdministraGrupo" name="AdministraGrupo">
                                    <?php AuxiliaSimNao($usuarioGrupo->GetAdministraGrupo()); ?>                        
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="Atividade">Ativo</label>
                                <select class="form-control" id="Ativo" name="Ativo">
                                    <?php AuxiliaSimNao($usuarioGrupo->GetAtivo()); ?>                        
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</body>