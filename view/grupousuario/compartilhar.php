<?php
    include_once '../compartilhado/sharedLogado.php';
    include_once '../../controller/grupo/listar.php';

    $get = $_GET;
    
    $idGrupo = $get['IdGrupo'];
    $Grupo = BuscarGrupoId($idGrupo);
    $nomeGrupo = $Grupo->GetGrupo();
    
    if(PodeCompartilhar($idGrupo) == 0) echo '<script>window.location.href="'.BASE_URL.'view/compartilhado/acessonegado.php"</script>';
?>

<body>
    <div class="container mt-5 mb-5">
    <br/>
    
        <div class="card border-primary">
            <div class="card-header text-center text-white bg-primary">Compartilhar Grupo</div>
            <div class="card-body">
                <form action="../../controller/usuariogrupo/inserir.php" method="POST">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="Email">E-mail do Usuário</label>
                                <input type="text" class="form-control" id="Email" name="Email">
                            </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="IdGrupo">Grupo</label>
                                    <select id="IdGrupo" name="IdGrupo" class="form-control">
                                        <?php echo '<option value="'.$idGrupo.'">'.$nomeGrupo.'</option>';?>
                                    </select>
                                </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Compartilhar</button>
                </form>
            </div>
        </div>
    </div>
</body>