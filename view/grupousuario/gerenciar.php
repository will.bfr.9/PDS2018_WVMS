<?php
    include_once '../compartilhado/sharedLogado.php';
    include_once '../../controller/prioridade/listar.php';
    include_once '../../controller/grupo/listar.php';

    $get = $_GET;
    
    $idGrupo = $get['IdGrupo'];
    $nomeGrupo = BuscarGrupoId($idGrupo);
    
    if(AdministraGrupo($idGrupo) == 0) echo '<script>window.location.href="'.BASE_URL.'view/compartilhado/acessonegado.php"</script>';
?>

<body>
    <div class="container mt-5 mb-5">
    <br/>
    <table class="table table-striped table-bordered">
            <thead>
                <tr class="bg-primary">
                    <th scope="col">Membro</th>
                    <th scope="col">Email</th>
                    <th scope="col">Pode Compartilhar</th>
                    <th scope="col">Atribui Atividades</th>
                    <th scope="col">Administra Grupo</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $lista = RetornaMembrosGrupo($idGrupo);
                    if(is_array($lista))
                    {
                        foreach($lista as $item)
                        {
                            echo '<tr>';
                                echo '<td>'.$item->GetNomeUsuario().'</td>';
                                echo '<td>'.$item->GetEmail().'</td>';
                                if($item->GetPodeCompartilhar() == true)
                                    echo '<td>Sim</td>';
                                else
                                    echo '<td>Não</td>';
                                if($item->GetAtribuiAtividade() == true)
                                    echo '<td>Sim</td>';
                                else
                                    echo '<td>Não</td>';
                                    if($item->GetAdministraGrupo() == true)
                                    echo '<td>Sim</td>';
                                else
                                    echo '<td>Não</td>';
                                echo '<td class="text-center pr-4"><a href="../grupousuario/editar.php?IdUsuarioGrupo='.$item->GetIdUsuarioGrupo().'">Permissões</a></td>';
                            echo '</tr>';
                        }
                    }
                    else
                    {

                    }
                ?>
            </tbody>
        </table>
    </div>
</body>