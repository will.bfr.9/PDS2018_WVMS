<?php
    include_once '../compartilhado/sharedLogado.php';
?>

<body>
    <div class="container mt-5 mb-5">
    <br/>
        <table class="table table-striped table-bordered">
            <thead>
                <tr class="bg-primary">
                    <th scope="col">Grupo</th>
                    <th scope="col">Descricao</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $lista = RetornaGrupos();
                    if(is_array($lista))
                    {
                        foreach($lista as $item)
                        {
                            echo '<tr>';
                                echo '<td>'.$item->GetNomeGrupo().'</td>';
                                echo '<td>'.$item->GetDescricaoGrupo().'</td>';
                                echo '<td class="text-right pr-4"><a href="../grupousuario/compartilhar.php?IdGrupo='.$item->GetIdGrupo().'">Compartilhar</a></td>';
                                echo '<td class="text-right pr-4"><a href="../atividades/novo.php?IdGrupo='.$item->GetIdGrupo().'">Nova Atividade</a></td>';
                                echo '<td class="text-right pr-4"><a href="../grupousuario/gerenciar.php?IdGrupo='.$item->GetIdGrupo().'">Gerenciar</a></td>';
                            echo '</tr>';
                        }
                    }
                    else
                    {

                    }
                ?>
            </tbody>
        </table>
    </div>
</body>