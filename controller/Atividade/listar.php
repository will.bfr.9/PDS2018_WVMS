<?php
    include_once '../../model/modelAtividade.php';
    include_once '../../config/config.php';

    function AtividadeRapida()
    {
        $modelAtividade = new ModelAtividade();

        $retorno = $modelAtividade->AtividadeRapida($_SESSION['logado'][0]);

        return $retorno;
    }

    function RetornaQtdAtividadesAbertas()
    {
        $modelAtividade = new ModelAtividade();

        $retorno = $modelAtividade->RetornaQtdAtividadesAbertas($_SESSION['logado'][0]);

        return $retorno;
    }

    function RetornaAtividadesAtribuidasUsuario()
    {
        $modelAtividade = new ModelAtividade();

        $retorno = $modelAtividade->RetornaAtividadesAtribuidasUsuario($_SESSION['logado'][0]);

        return $retorno;
    }
    function RetornaAtividadesCriadasUsuario()
    {
        $modelAtividade = new ModelAtividade();

        $retorno = $modelAtividade->RetornaAtividadesCriadasUsuario($_SESSION['logado'][0]);

        return $retorno;
    }
    function BuscaAtividadePorId($idAtividade)
    {
        $modelAtividade = new ModelAtividade();

        $retorno = $modelAtividade->BuscaAtividadePorId($idAtividade);

        return $retorno[0];
    }
    function VerificaUsuarioAtribuidoAtividade($idAtividade)
    {
        $modelAtividade = new ModelAtividade();

        $retorno = $modelAtividade->VerificaUsuarioAtribuidoAtividade($idAtividade, $_SESSION['logado'][0]);

        return $retorno[0];
    }

    function VerificaUsuarioCriadorAtividade($idAtividade)
    {
        $modelAtividade = new ModelAtividade();

        $retorno = $modelAtividade->VerificaUsuarioCriadorAtividade($idAtividade, $_SESSION['logado'][0]);

        return $retorno[0];
    }

    function RetornaQtdAtividadesParaAvaliar()
    {
        $modelAtividade = new ModelAtividade();

        $retorno = $modelAtividade->RetornaQtdAtividadesParaAvaliar($_SESSION['logado'][0]);

        return $retorno;
    }
    function RetornaAtividadesCriadasParaAvaliar()
    {
        $modelAtividade = new ModelAtividade();

        $retorno = $modelAtividade->RetornaAtividadesCriadasParaAvaliar($_SESSION['logado'][0]);

        return $retorno;
    }
?>