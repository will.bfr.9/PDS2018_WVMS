<?php
    include_once '../../model/modelAtividade.php';
    include_once '../../config/config.php';

    $post = $_POST;


    $atividade = new Atividade();

    $atividade->SetIdAtividade($post['IdAtividade']);
    $atividade->SetAtividade($post['Atividade']);
    $atividade->SetAtivo(1);
    $atividade->SetDataCriacao($post['DataCriacao']);
    $atividade->SetDescricao($post['Descricao']);
    $atividade->SetIdUsuarioAtribuido($post['IdUsuarioAtribuido']);
    $atividade->SetIdGrupo($post['IdGrupo']);
    $atividade->SetIdCriador($post['IdCriador']);
    $atividade->SetIdPrioridade($post['IdPrioridade']);
    $atividade->SetIdStatus($post['IdStatus']);
    $atividade->SetIdAvaliacao($post['IdAvaliacao']);

    $modelAtividade = new ModelAtividade();

    $retorno = $modelAtividade->Avaliar($atividade);

    if($retorno > 0)
    {
        echo '<script>window.location.href="'.BASE_URL.'view/atividades/listaravaliar.php"</script>';
    }
    else
    {
        
        echo '<script>swal("Ocorreu um erro ao avaliar a atividade.", "Ocorreu um erro!", "error").then((value) => {
            window.location.href="'.BASE_URL.'view/atividades/avaliar.php?IdAtividade='.$atividade->GetIdAtividade().'";
        }); </script>';
    }

    
?>