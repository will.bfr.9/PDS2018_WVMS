<?php
    include_once '../../model/modelAtividade.php';
    include_once '../../view/compartilhado/sharedLimpo.php';
    
    $post = $_POST;


    $atividade = new Atividade();

    $atividade->SetIdAtividade($post['IdAtividade']);
    $atividade->SetAtividade($post['Atividade']);
    $atividade->SetAtivo(1);
    $atividade->SetDataCriacao($post['DataCriacao']);
    $atividade->SetDescricao($post['Descricao']);
    $atividade->SetIdUsuarioAtribuido($post['IdUsuarioAtribuido']);
    $atividade->SetIdGrupo($post['IdGrupo']);
    $atividade->SetIdCriador($post['IdCriador']);
    $atividade->SetIdPrioridade($post['IdPrioridade']);
    $atividade->SetIdStatus($post['IdStatus']);
    if($post['IdStatus'] == 3)
    {
        $atividade->SetDataEntrega(date('Y-m-d'));
    }
    else
    {
        $atividade->SetDataEntrega(null);
    }

    $modelAtividade = new ModelAtividade();

    $retorno = $modelAtividade->Atualizar($atividade);

    if($retorno > 0)
    {
        echo '<script>window.location.href="'.BASE_URL.'view/atividades/listar.php"</script>';
    }
    else
    {
        echo '<script>swal("Ocorreu um erro ao atualizar a atividade.", "Ocorreu um erro!", "error").then((value) => {
            window.location.href="'.BASE_URL.'view/atividades/atualizarStatus.php?IdAtividade='.$atividade->GetIdAtividade().'";
        }); </script>';
    }

    
?>