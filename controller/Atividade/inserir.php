<?php
    include_once '../../model/modelAtividade.php';
    include_once '../../view/compartilhado/sharedLimpo.php';

    $post = $_POST;

    $atividade = new Atividade();

    $atividade->SetAtividade($post['Atividade']);
    $atividade->SetAtivo(1);
    $atividade->SetDataCriacao(date('Y-m-d'));
    $atividade->SetDescricao($post['Descricao']);
    $atividade->SetIdUsuarioAtribuido($post['IdUsuarioAtribuido']);
    $atividade->SetIdGrupo($post['IdGrupo']);
    $atividade->SetIdCriador($_SESSION['logado'][0]);
    $atividade->SetIdPrioridade($post['IdPrioridade']);
    $atividade->SetIdStatus(1);

    if(strlen(trim($atividade->GetAtividade())) < 5 )
    {
        echo '<script>swal("O modelo não está válido.", "Ocorreu um erro!", "error").then((value) => {
            window.location.href="'.BASE_URL.'view/atividades/novo.php?IdGrupo='.$post['IdGrupo'].'";
        }); </script>';
    }
    else
    {
        $modelAtividade = new ModelAtividade();

        $retorno = $modelAtividade->Inserir($atividade);

        if($retorno > 0)
        {
            echo '<script>window.location.href="'.BASE_URL.'view/atividades/listar.php"</script>';
        }
        else
        {
            echo '<script>swal("Erro ao cadastrar Atividade no sistema.", "Ocorreu um erro!", "error").then((value) => {
                window.location.href="'.BASE_URL.'view/atividades/novo.php?IdGrupo='.$post['IdGrupo'].'";
            }); </script>';
        }

    }
    
?>