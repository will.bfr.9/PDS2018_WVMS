<?php
    include_once '../../model/modelUsuarioGrupo.php';
    include_once '../Usuario/listar.php';
    include_once '../../view/compartilhado/sharedLimpo.php';

    $post = $_POST;

    $usuarioGrupo = new UsuarioGrupo();

    $usuarioGrupo->SetIdUsuarioGrupo($post['IdUsuarioGrupo']);
    $usuarioGrupo->SetIdUsuario($post['IdUsuario']);
    $usuarioGrupo->SetIdGrupo($post['IdGrupo']);
    $usuarioGrupo->SetAtribuiAtividade($post['AtribuiAtividade']);
    $usuarioGrupo->SetPodeCompartilhar($post['PodeCompartilhar']);
    $usuarioGrupo->SetAdministraGrupo($post['AdministraGrupo']);
    $usuarioGrupo->SetAtivo($post['Ativo']);

    $modelUsuarioGrupo = new ModelUsuarioGrupo();

    $retorno = $modelUsuarioGrupo->Atualizar($usuarioGrupo);

    if($retorno > 0)
    {
        echo '<script>swal("Privilégios atualizados com sucesso.", "Sucesso!", "success").then((value) => {
            window.location.href="'.BASE_URL.'view/grupousuario/gerenciar.php?IdGrupo='.$usuarioGrupo->GetIdGrupo().'";
        }); </script>';
    }
    else
    {
        echo '<script>swal("Ocorreu um erro ao atualizar os privilégios Ou você não alterou os privilégios deste usuário.", "Ocorreu um erro!", "error").then((value) => {
            window.location.href="'.BASE_URL.'view/grupousuario/editar.php?IdUsuarioGrupo='.$usuarioGrupo->GetIdUsuarioGrupo().'";
        }); </script>';
    }

    
?>