<?php
    include_once '../../model/modelUsuarioGrupo.php';
    include_once '../../config/config.php';

    function RetornaQtd()
    {
        $modelUsuarioGrupo = new ModelUsuarioGrupo();

        $retorno = $modelUsuarioGrupo->RetornaQtdGrupos($_SESSION['logado'][0]);

        return $retorno;
    }

    function RetornaGrupos()
    {
        $modelUsuarioGrupo = new ModelUsuarioGrupo();

        $retorno = $modelUsuarioGrupo->RetornaGrupos($_SESSION['logado'][0]);

        return $retorno;
    }

    function PodeAtribuirAtividade($idGrupo)
    {
        $modelUsuarioGrupo = new ModelUsuarioGrupo();

        $retorno = $modelUsuarioGrupo->PodeAtribuirAtividade($_SESSION['logado'][0], $idGrupo);

        return $retorno;
    }

    function PodeCompartilhar($idGrupo)
    {
        $modelUsuarioGrupo = new ModelUsuarioGrupo();

        $retorno = $modelUsuarioGrupo->PodeCompartilhar($_SESSION['logado'][0], $idGrupo);

        return $retorno;
    }
    
    function AdministraGrupo($idGrupo)
    {
        $modelUsuarioGrupo = new ModelUsuarioGrupo();

        $retorno = $modelUsuarioGrupo->AdministraGrupo($_SESSION['logado'][0], $idGrupo);

        return $retorno;
    }

    function RetornaMembrosGrupo($idGrupo)
    {
        $modelUsuarioGrupo = new ModelUsuarioGrupo();

        $retorno = $modelUsuarioGrupo->RetornaMembrosGrupo($idGrupo);

        return $retorno;
    }

    function RetornaUsuarioGrupo($idUsuarioGrupo)
    {
        $modelUsuarioGrupo = new ModelUsuarioGrupo();

        $retorno = $modelUsuarioGrupo->RetornaUsuarioGrupo($idUsuarioGrupo);

        return $retorno[0];
    }
?>