<?php
    include_once '../../model/modelUsuarioGrupo.php';
    include_once '../Usuario/listar.php';
    include_once '../../view/compartilhado/sharedLimpo.php';

    $post = $_POST;

    $user = BuscarUsuarioPorEmail($post['Email']);

    if($user==null)
    {
        echo '<script>swal("Usuário não encontrado Ou Você inseriu seu E-mail.", "Ocorreu um erro!", "error").then((value) => {
            window.location.href="'.BASE_URL.'view/grupousuario/compartilhar.php?IdGrupo='.$post['IdGrupo'].'";
        }); </script>';
    }
    else
    {
        $usuarioGrupo = new UsuarioGrupo();

        $usuarioGrupo->SetIdUsuario($user->GetIdUsuario());
        $usuarioGrupo->SetIdGrupo($post['IdGrupo']);
        $usuarioGrupo->SetAtribuiAtividade(0);
        $usuarioGrupo->SetPodeCompartilhar(0);
        $usuarioGrupo->SetAdministraGrupo(0);
        $usuarioGrupo->SetAtivo(1);

        $modelUsuarioGrupo = new ModelUsuarioGrupo();

        $retorno = $modelUsuarioGrupo->Inserir($usuarioGrupo);

        if($retorno > 0)
        {
            echo '<script>swal("Grupo Compartilhado com sucesso.", "Sucesso!", "success").then((value) => {
                window.location.href="'.BASE_URL.'view/atividades/listar.php";
            }); </script>';
        }
        else
        {
            echo '<script>swal("Erro ao compartilhar grupo no sistema.", "Ocorreu um erro!", "error").then((value) => {
                window.location.href="'.BASE_URL.'view/grupousuario/compartilhar.php?IdGrupo='.$post['IdGrupo'].'";
            }); </script>';
        }

    }
    
?>