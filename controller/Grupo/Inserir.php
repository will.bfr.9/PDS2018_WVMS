<?php
    include_once '../../model/modelGrupo.php';
    include_once '../../view/compartilhado/sharedLimpo.php';

    $post = $_POST;

    $grupo = new GrupoAtividade();

    $grupo->SetGrupo($post['Grupo']);
    $grupo->SetIdUsuario($_SESSION['logado'][0]);
    $grupo->SetDescricao($post['Descricao']);
    $grupo->SetAtivo(true);

    if(strlen(trim($grupo->GetGrupo())) < 5 )
    {
        echo '<script>swal("O modelo não está válido.", "Ocorreu um erro!", "error").then((value) => {
            window.location.href="'.BASE_URL.'view/grupo/novo.php";
        }); </script>';
    }
    else
    {
        $modelGrupo = new ModelGrupo();

        $retorno = $modelGrupo->Inserir($grupo);

        if($retorno > 0)
        {
            echo '<script>window.location.href="'.BASE_URL.'view/atividades/listar.php"</script>';
        }
        else
        {
            echo '<script>swal("Erro ao cadastrar grupo no sistema.", "Ocorreu um erro!", "error").then((value) => {
                window.location.href="'.BASE_URL.'view/grupo/novo.php";
            }); </script>';
        }

    }
    
?>