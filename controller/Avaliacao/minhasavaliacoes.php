<?php
    include_once '../../model/modelAvaliacao.php';
    include_once '../../config/config.php';

    function AtividadesTotal()
    {
        $modelAvaliacao = new ModelAvaliacao();

        $retorno = $modelAvaliacao->AtividadesTotal($_SESSION['logado'][0]);

        return $retorno;
    }
    function AtividadesFeitas()
    {
        $modelAvaliacao = new ModelAvaliacao();

        $retorno = $modelAvaliacao->AtividadesFeitas($_SESSION['logado'][0]);

        return $retorno;
    }

    function AtividadesAvaliadas()
    {
        $modelAvaliacao = new ModelAvaliacao();

        $retorno = $modelAvaliacao->AtividadesAvaliadas($_SESSION['logado'][0]);

        return $retorno;
    }
    function AtividadesPorAvaliacao($IdAvaliacao)
    {
        $modelAvaliacao = new ModelAvaliacao();

        $retorno = $modelAvaliacao->AtividadesPorAvaliacao($_SESSION['logado'][0], $IdAvaliacao);

        return $retorno;
    }
    
?>