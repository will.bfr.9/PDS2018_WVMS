<?php
    include_once '../../model/modelUsuario.php';
    include_once '../../view/compartilhado/sharedLimpo.php';

    $post = $_POST;

    $login = $post['Email'];
    $senha = $post['Senha'];

    $modelUsuario = new ModelUsuario();
    $resultado = $modelUsuario->Logar($login, $senha);

    if(empty($resultado))
    {
        session_unset();
        echo '<script>swal("Usuário e/ou senha Inválidos.", "Ocorreu um erro!", "error").then((value) => {
            window.location.href="'.BASE_URL.'view/home/index.php";
        }); </script>';
    }
    else
    {
        $sessaoArray = array($resultado['IdUsuario'], $resultado['NomeUsuario'], $resultado['Email']);
        $_SESSION['logado'] = $sessaoArray;
        echo '<script>window.location.href="'.BASE_URL.'view/atividades/listar.php";</script>';
    }

?>