<?php
    include_once '../../model/modelUsuario.php';
    include_once '../../config/config.php';

    function BuscarUsuarioPorEmail($email)
    {
        $modelUsuario = new ModelUsuario();

        $retorno = $modelUsuario->BuscarUsuarioPorEmail($email);

        if(is_array($retorno))
        {
            if($retorno[0]->GetEmail() == $_SESSION['logado'][2])
            {
                $retorno = null;
            }
    
            return $retorno[0];
        }
        
        return null;
    }
    
?>