<?php
    include_once '../../model/modelUsuario.php';
    include_once '../../view/compartilhado/sharedLimpo.php';

    $post = $_POST;

    $usuario = new Usuario();

    $usuario->SetNomeUsuario($post['NomeUsuario']);
    $usuario->SetSenha($post['Senha']);
    $usuario->SetEmail($post['Email']);
    $usuario->SetAtivo(true);

    if(strlen(trim($usuario->GetNomeUsuario())) < 5 || strlen(trim($usuario->GetSenha())) < 5 || strlen(trim($usuario->GetEmail())) < 5)
    {
        echo '<script>swal("O modelo não está válido.", "Ocorreu um erro!", "error").then((value) => {
            window.location.href="'.BASE_URL.'view/home/cadastro.php";
        }); </script>';
    }
    else
    {
        $modelUsuario = new modelUsuario();

        $retorno = $modelUsuario->EncontrarEmail($usuario->getEmail());

    if($retorno == true)
    {
        $retorno = $modelUsuario->Inserir($usuario);

        if($retorno > 0)
        {
            echo '<script>swal("Usuário cadastrado com sucesso.", "Sucesso!", "success").then((value) => {
                window.location.href="'.BASE_URL.'view/home/index.php";
            }); </script>';
        }
        else
        {
            echo '<script>swal("erro ao cadastrar usuário no sistema.", "Ocorreu um erro!", "error").then((value) => {
                window.location.href="'.BASE_URL.'view/home/cadastro.php";
            }); </script>';
        }
    }
    else
    {
        echo '<script>swal("Já existe esse e-mail cadastrado no sistema.", "Ocorreu um erro!", "error").then((value) => {
            window.location.href="'.BASE_URL.'view/home/cadastro.php";
        }); </script>';
    }

}
    
?>