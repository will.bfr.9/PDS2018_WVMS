<?php
    include_once 'modelBase.php';
    include_once 'entity/prioridadeAtividade.php';

    class ModelPrioridade
    {
        public function RetornaPrioridade()
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT * FROM prioridadeAtividade WHERE Ativo = 1;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                }
            }

            FecharConexao($con);

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $prioridade = new PrioridadeAtividade();
                    $prioridade->SetIdPrioridade($row['IdPrioridade']);
                    $prioridade->SetPrioridade($row['Prioridade']);
                    $prioridade->SetAtivo($row['Ativo']);
    
                    $listaRetorno[] = $prioridade;
                }
            }
        
            return $listaRetorno;
        }

    }
    
?>