<?php
    include_once 'modelBase.php';
    include_once 'entity/usuarioGrupo.php';

    class ModelUsuarioGrupo
    {
        public function RetornaQtdGrupos($idUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdUsuarioGrupo) AS 'qtd' FROM usuariogrupo WHERE IdUsuario = $idUsuario;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }
        public function RetornaGrupos($idUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT grupoatividade.Grupo, grupoatividade.Descricao, usuariogrupo.* FROM usuariogrupo INNER JOIN grupoatividade ON grupoatividade.IdGrupo = usuariogrupo.IdGrupo WHERE usuariogrupo.IdUsuario = $idUsuario;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                }
            }

            FecharConexao($con);

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $usuarioGrupo = new UsuarioGrupo();
                    $usuarioGrupo->SetIdUsuarioGrupo($row['IdUsuarioGrupo']);
                    $usuarioGrupo->SetIdGrupo($row['IdGrupo']);
                    $usuarioGrupo->SetAtivo($row['Ativo']);
                    $usuarioGrupo->SetIdUsuario($row['IdUsuario']);
                    $usuarioGrupo->SetAdministraGrupo($row['AdministraGrupo']);
                    $usuarioGrupo->SetAtribuiAtividade($row['AtribuiAtividade']);
                    $usuarioGrupo->SetPodeCompartilhar($row['PodeCompartilhar']);
                    $usuarioGrupo->SetNomeGrupo($row['Grupo']);
                    $usuarioGrupo->SetDescricaoGrupo($row['Descricao']);
                    $listaRetorno[] = $usuarioGrupo;
                }
            }
        
            return $listaRetorno;
        }

        public function PodeAtribuirAtividade($idUsuario, $idGrupo)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdUsuarioGrupo) AS qtd FROM usuariogrupo WHERE idUsuario = $idUsuario AND AtribuiAtividade = 1 AND IdGrupo = $idGrupo;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }

        public function PodeCompartilhar($idUsuario, $idGrupo)
        {
            $con = AbrirConexao();
            
            $query = "SELECT COUNT(IdUsuarioGrupo) AS qtd FROM usuariogrupo WHERE idUsuario = $idUsuario AND PodeCompartilhar = 1 AND IdGrupo = $idGrupo;";
            
            
            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }
    
        public function AdministraGrupo($idUsuario, $idGrupo)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdUsuarioGrupo) AS qtd FROM usuariogrupo WHERE idUsuario = $idUsuario AND AdministraGrupo = 1 AND IdGrupo = $idGrupo;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }

        public function RetornaMembrosGrupo($idGrupo)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT usuario.Email, usuario.NomeUsuario, usuariogrupo.* FROM usuariogrupo INNER JOIN usuario ON usuario.IdUsuario = usuariogrupo.IdUsuario WHERE usuariogrupo.IdGrupo = $idGrupo AND usuariogrupo.Ativo=1;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                }
            }

            FecharConexao($con);

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $usuarioGrupo = new UsuarioGrupo();
                    $usuarioGrupo->SetIdUsuarioGrupo($row['IdUsuarioGrupo']);
                    $usuarioGrupo->SetIdGrupo($row['IdGrupo']);
                    $usuarioGrupo->SetAtivo($row['Ativo']);
                    $usuarioGrupo->SetIdUsuario($row['IdUsuario']);
                    $usuarioGrupo->SetAdministraGrupo($row['AdministraGrupo']);
                    $usuarioGrupo->SetAtribuiAtividade($row['AtribuiAtividade']);
                    $usuarioGrupo->SetPodeCompartilhar($row['PodeCompartilhar']);
                    $usuarioGrupo->SetNomeUsuario($row['NomeUsuario']);
                    $usuarioGrupo->SetEmail($row['Email']);
                    $listaRetorno[] = $usuarioGrupo;
                }
            }
        
            return $listaRetorno;
        }

        public function Inserir(UsuarioGrupo $UsuarioGrupo)
        {
            $idUsuario = $UsuarioGrupo->GetIdUsuario();
            $idGrupo = $UsuarioGrupo->GetIdGrupo();
            $atribuiAtividade = $UsuarioGrupo->GetAtribuiAtividade();
            $podeCompartilhar = $UsuarioGrupo->GetPodeCompartilhar();
            $administraGrupo = $UsuarioGrupo->GetAdministraGrupo();
            $ativo = $UsuarioGrupo->GetAtivo();

            $con = AbrirConexao();
                
            
            $query = "INSERT INTO UsuarioGrupo (IdUsuario, IdGrupo, AtribuiAtividade, 
                    PodeCompartilhar, AdministraGrupo, Ativo) VALUES ('$idUsuario', '$idGrupo', 
                    '$atribuiAtividade','$podeCompartilhar', '$administraGrupo', '$ativo');";

            $executa = mysqli_query($con, $query);

            $numLinha = mysqli_affected_rows($con);

            FecharConexao($con);
        
            return $numLinha;
        }

        public function RetornaUsuarioGrupo($idUsuarioGrupo)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT grupoatividade.Grupo, usuario.Email, usuario.NomeUsuario, 
                      usuariogrupo.* FROM usuariogrupo INNER JOIN 
                      usuario ON usuario.IdUsuario = usuariogrupo.IdUsuario INNER JOIN
                      grupoatividade ON grupoatividade.IdGrupo = usuariogrupo.IdGrupo WHERE 
                      usuariogrupo.IdUsuarioGrupo = $idUsuarioGrupo AND usuariogrupo.Ativo=1;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                }
            }

            FecharConexao($con);

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $usuarioGrupo = new UsuarioGrupo();
                    $usuarioGrupo->SetIdUsuarioGrupo($row['IdUsuarioGrupo']);
                    $usuarioGrupo->SetIdGrupo($row['IdGrupo']);
                    $usuarioGrupo->SetAtivo($row['Ativo']);
                    $usuarioGrupo->SetIdUsuario($row['IdUsuario']);
                    $usuarioGrupo->SetAdministraGrupo($row['AdministraGrupo']);
                    $usuarioGrupo->SetAtribuiAtividade($row['AtribuiAtividade']);
                    $usuarioGrupo->SetPodeCompartilhar($row['PodeCompartilhar']);
                    $usuarioGrupo->SetNomeUsuario($row['NomeUsuario']);
                    $usuarioGrupo->SetEmail($row['Email']);
                    $usuarioGrupo->SetNomeGrupo($row['Grupo']);
                    $listaRetorno[] = $usuarioGrupo;
                }
            }
        
            return $listaRetorno;
        }

        public function Atualizar(UsuarioGrupo $UsuarioGrupo)
        {
            $idUsuarioGrupo = $UsuarioGrupo->GetIdUsuarioGrupo();
            $idUsuario = $UsuarioGrupo->GetIdUsuario();
            $idGrupo = $UsuarioGrupo->GetIdGrupo();
            $atribuiAtividade = $UsuarioGrupo->GetAtribuiAtividade();
            $podeCompartilhar = $UsuarioGrupo->GetPodeCompartilhar();
            $administraGrupo = $UsuarioGrupo->GetAdministraGrupo();
            $ativo = $UsuarioGrupo->GetAtivo();

            $con = AbrirConexao();
                
            
            $query = "UPDATE UsuarioGrupo 
                    SET IdUsuario = $idUsuario, IdGrupo = $idGrupo, 
                    AtribuiAtividade = $atribuiAtividade, 
                    PodeCompartilhar = $podeCompartilhar, 
                    AdministraGrupo = $administraGrupo, 
                    Ativo = $ativo WHERE IdUsuarioGrupo = $idUsuarioGrupo;";

            $executa = mysqli_query($con, $query);

            $numLinha = mysqli_affected_rows($con);

            FecharConexao($con);
        
            return $numLinha;
        }
    }
    
?>