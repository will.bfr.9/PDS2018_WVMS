<?php
    include_once 'modelBase.php';
    include_once 'entity/statusAtividade.php';

    class ModelStatus
    {
        public function RetornaStatus()
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT * FROM statusatividade WHERE Ativo = 1;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                }
            }

            FecharConexao($con);

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $status = new StatusAtividade();
                    $status->SetIdStatus($row['IdStatus']);
                    $status->SetStatus($row['NomeStatus']);
                    $status->SetAtivo($row['Ativo']);
    
                    $listaRetorno[] = $status;
                }
            }
        
            return $listaRetorno;
        }

    }
    
?>