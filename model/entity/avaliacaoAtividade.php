<?php

    class AvaliacaoAtividade
    {
        private $IdAvaliacao;
        private $Avaliacao;
        private $Ativo;

        public function SetIdAvaliacao($IdAvaliacao)
        {
            $this->IdAvaliacao = $IdAvaliacao;
        }

        public function GetIdAvaliacao()
        {
            return $this->IdAvaliacao;
        }

        public function SetAvaliacao($Avaliacao)
        {
            $this->Avaliacao = $Avaliacao;
        }

        public function GetAvaliacao()
        {
            return $this->Avaliacao;
        }

        public function SetAtivo($Ativo)
        {
            $this->Ativo = $Ativo;
        }
        public function GetAtivo()
        {
            return $this->Ativo;
        }
    }
?>