<?php

    class PrioridadeAtividade
    {
        private $IdPrioridade;
        private $Prioridade;
        private $Ativo;

        public function SetIdPrioridade($IdPrioridade)
        {
            $this->IdPrioridade = $IdPrioridade;
        }

        public function GetIdPrioridade()
        {
            return $this->IdPrioridade;
        }

        public function SetPrioridade($Prioridade)
        {
            $this->Prioridade = $Prioridade;
        }

        public function GetPrioridade()
        {
            return $this->Prioridade;
        }

        public function SetAtivo($Ativo)
        {
            $this->Ativo = $Ativo;
        }
        public function GetAtivo()
        {
            return $this->Ativo;
        }
    }
?>