<?php

    class Usuario
    {
        private $IdUsuario;
        private $NomeUsuario;
        private $Ativo;
        private $Email;
        private $Senha;

        public function SetIdUsuario($IdUsuario)
        {
            $this->IdUsuario = $IdUsuario;
        }

        public function GetIdUsuario()
        {
            return $this->IdUsuario;
        }

        public function SetNomeUsuario($NomeUsuario)
        {
            $this->NomeUsuario = $NomeUsuario;
        }
       
        public function GetNomeUsuario()
        {
            return $this->NomeUsuario;
        }

        public function SetAtivo($Ativo)
        {
            $this->Ativo = $Ativo;
        }
        public function GetAtivo()
        {
            return $this->Ativo;
        }

        public function SetEmail($Email)
        {
            $this->Email = $Email;
        }
        public function GetEmail()
        {
            return $this->Email;
        }

        public function SetSenha($Senha)
        {
            $this->Senha = $Senha;
        }
        public function GetSenha()
        {
            return $this->Senha;
        }
    }
?>