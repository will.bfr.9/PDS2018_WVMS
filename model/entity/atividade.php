<?php

    class Atividade
    {
        private $IdAtividade;
        private $Atividade;
        private $Ativo;
        private $DataCriacao;
        private $DataEntrega;
        private $Descricao;
        private $IdAvaliacao;
        private $IdCriador;
        private $IdGrupo;
        private $IdPrioridade;
        private $IdStatus;
        private $IdUsuarioAtribuido;

        private $NomeGrupo; //Para Inner Join
        private $DescricaoGrupo; //Para Inner Join
        private $NomeCriador; //Para Inner Join
        private $EmailCriador; //Para Inner Join
        private $NomeAtribuido; //Para Inner Join
        private $EmailAtribuido; //Para Inner Join

        private $NomeStatus; //Para Inner Join
        private $NomePrioridade; //Para Inner Join

        public function SetNomePrioridade($NomePrioridade)
        {
            $this->NomePrioridade = $NomePrioridade;
        }

        public function GetNomePrioridade()
        {
            return $this->NomePrioridade;
        }

        public function SetNomeStatus($NomeStatus)
        {
            $this->NomeStatus = $NomeStatus;
        }

        public function GetNomeStatus()
        {
            return $this->NomeStatus;
        }

        public function SetEmailAtribuido($EmailAtribuido)
        {
            $this->EmailAtribuido = $EmailAtribuido;
        }

        public function GetEmailAtribuido()
        {
            return $this->EmailAtribuido;
        }

        public function SetNomeAtribuido($NomeAtribuido)
        {
            $this->NomeAtribuido = $NomeAtribuido;
        }

        public function GetNomeAtribuido()
        {
            return $this->NomeAtribuido;
        }

        //aqui
        public function SetEmailCriador($EmailCriador)
        {
            $this->EmailCriador = $EmailCriador;
        }

        public function GetEmailCriador()
        {
            return $this->EmailCriador;
        }

        public function SetNomeCriador($NomeCriador)
        {
            $this->NomeCriador = $NomeCriador;
        }

        public function GetNomeCriador()
        {
            return $this->NomeCriador;
        }

        public function SetDescricaoGrupo($DescricaoGrupo)
        {
            $this->DescricaoGrupo = $DescricaoGrupo;
        }

        public function GetDescricaoGrupo()
        {
            return $this->DescricaoGrupo;
        }

        public function SetNomeGrupo($NomeGrupo)
        {
            $this->NomeGrupo = $NomeGrupo;
        }

        public function GetNomeGrupo()
        {
            return $this->NomeGrupo;
        }

        public function SetIdAtividade($IdAtividade)
        {
            $this->IdAtividade = $IdAtividade;
        }

        public function GetIdAtividade()
        {
            return $this->IdAtividade;
        }

        public function SetAtividade($Atividade)
        {
            $this->Atividade = $Atividade;
        }
       
        public function GetAtividade()
        {
            return $this->Atividade;
        }

        public function SetAtivo($Ativo)
        {
            $this->Ativo = $Ativo;
        }
        public function GetAtivo()
        {
            return $this->Ativo;
        }

        public function SetDataCriacao($DataCriacao)
        {
            $this->DataCriacao = $DataCriacao;
        }
        public function GetDataCriacao()
        {
            return $this->DataCriacao;
        }

        public function SetDataEntrega($DataEntrega)
        {
            $this->DataEntrega = $DataEntrega;
        }
        public function GetDataEntrega()
        {
            return $this->DataEntrega;
        }

        public function SetDescricao($Descricao)
        {
            $this->Descricao = $Descricao;
        }
        public function GetDescricao()
        {
            return $this->Descricao;
        }

        public function SetIdAvaliacao($IdAvaliacao)
        {
            $this->IdAvaliacao = $IdAvaliacao;
        }
        public function GetIdAvaliacao()
        {
            return $this->IdAvaliacao;
        }

        public function SetIdCriador($IdCriador)
        {
            $this->IdCriador = $IdCriador;
        }
        public function GetIdCriador()
        {
            return $this->IdCriador;
        }

        public function SetIdGrupo($IdGrupo)
        {
            $this->IdGrupo = $IdGrupo;
        }
        public function GetIdGrupo()
        {
            return $this->IdGrupo;
        }

        public function SetIdPrioridade($IdPrioridade)
        {
            $this->IdPrioridade = $IdPrioridade;
        }
        public function GetIdPrioridade()
        {
            return $this->IdPrioridade;
        }

        public function SetIdStatus($IdStatus)
        {
            $this->IdStatus = $IdStatus;
        }
        public function GetIdStatus()
        {
            return $this->IdStatus;
        }

        public function SetIdUsuarioAtribuido($IdUsuarioAtribuido)
        {
            $this->IdUsuarioAtribuido = $IdUsuarioAtribuido;
        }
        public function GetIdUsuarioAtribuido()
        {
            return $this->IdUsuarioAtribuido;
        }
    }
?>