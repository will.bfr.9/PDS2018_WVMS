<?php

    class StatusAtividade
    {
        private $IdStatus;
        private $Status;
        private $Ativo;

        public function SetIdStatus($IdStatus)
        {
            $this->IdStatus = $IdStatus;
        }

        public function GetIdStatus()
        {
            return $this->IdStatus;
        }

        public function SetStatus($Status)
        {
            $this->Status = $Status;
        }

        public function GetStatus()
        {
            return $this->Status;
        }

        public function SetAtivo($Ativo)
        {
            $this->Ativo = $Ativo;
        }
        public function GetAtivo()
        {
            return $this->Ativo;
        }
    }
?>