<?php

    class AtividadeRecorrente
    {
        private $IdAtividadeRecorrente;
        private $IdAtividade;
        private $Ativo;
        private $Segunda;
        private $Terca;
        private $Quarta;
        private $Quinta;
        private $Sexta;
        private $Sabado;
        private $Domingo;

        public function SetIdAtividadeRecorrente($IdAtividadeRecorrente)
        {
            $this->IdAtividadeRecorrente = $IdAtividadeRecorrente;
        }

        public function GetIdAtividadeRecorrente()
        {
            return $this->IdAtividadeRecorrente;
        }

        public function SetIdAtividade($IdAtividade)
        {
            $this->IdAtividade = $IdAtividade;
        }

        public function GetIdAtividade()
        {
            return $this->IdAtividade;
        }

        public function SetAtivo($Ativo)
        {
            $this->Ativo = $Ativo;
        }
        public function GetAtivo()
        {
            return $this->Ativo;
        }

        public function SetSegunda($Segunda)
        {
            $this->Segunda = $Segunda;
        }
        public function GetSegunda()
        {
            return $this->Segunda;
        }

        public function SetTerca($Terca)
        {
            $this->Terca = $Terca;
        }
        public function GetTerca()
        {
            return $this->Terca;
        }

        public function SetQuarta($Quarta)
        {
            $this->Quarta = $Quarta;
        }
        public function GetQuarta()
        {
            return $this->Quarta;
        }

        public function SetQuinta($Quinta)
        {
            $this->Quinta = $Quinta;
        }
        public function GetQuinta()
        {
            return $this->Quinta;
        }

        public function SetSexta($Sexta)
        {
            $this->Sexta = $Sexta;
        }
        public function GetSexta()
        {
            return $this->Sexta;
        }

        public function SetSabado($Sabado)
        {
            $this->Sabado = $Sabado;
        }
        public function GetSabado()
        {
            return $this->Sabado;
        }

        public function SetDomingo($Domingo)
        {
            $this->Domingo = $Domingo;
        }
        public function GetDomingo()
        {
            return $this->Domingo;
        }
    }
?>