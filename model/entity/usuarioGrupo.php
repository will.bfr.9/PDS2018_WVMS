<?php

    class UsuarioGrupo
    {
        private $IdUsuarioGrupo;
        private $IdGrupo;
        private $Ativo;
        private $IdUsuario;
        private $AdministraGrupo;
        private $AtribuiAtividade;
        private $PodeCompartilhar;
        private $NomeGrupo; //Para Inner Join
        private $DescricaoGrupo; //Para Inner Join
        private $NomeUsuario; //Para Inner Join
        private $Email; //Para Inner Join

        public function SetEmail($Email)
        {
            $this->Email = $Email;
        }

        public function GetEmail()
        {
            return $this->Email;
        }

        public function SetNomeUsuario($NomeUsuario)
        {
            $this->NomeUsuario = $NomeUsuario;
        }

        public function GetNomeUsuario()
        {
            return $this->NomeUsuario;
        }

        public function SetDescricaoGrupo($DescricaoGrupo)
        {
            $this->DescricaoGrupo = $DescricaoGrupo;
        }

        public function GetDescricaoGrupo()
        {
            return $this->DescricaoGrupo;
        }

        public function SetNomeGrupo($NomeGrupo)
        {
            $this->NomeGrupo = $NomeGrupo;
        }

        public function GetNomeGrupo()
        {
            return $this->NomeGrupo;
        }

        public function SetIdUsuarioGrupo($IdUsuarioGrupo)
        {
            $this->IdUsuarioGrupo = $IdUsuarioGrupo;
        }

        public function GetIdUsuarioGrupo()
        {
            return $this->IdUsuarioGrupo;
        }

        public function SetIdGrupo($IdGrupo)
        {
            $this->IdGrupo = $IdGrupo;
        }
       
        public function GetIdGrupo()
        {
            return $this->IdGrupo;
        }

        public function SetAtivo($Ativo)
        {
            $this->Ativo = $Ativo;
        }
        public function GetAtivo()
        {
            return $this->Ativo;
        }

        public function SetIdUsuario($IdUsuario)
        {
            $this->IdUsuario = $IdUsuario;
        }
        public function GetIdUsuario()
        {
            return $this->IdUsuario;
        }

        public function SetAdministraGrupo($AdministraGrupo)
        {
            $this->AdministraGrupo = $AdministraGrupo;
        }
        public function GetAdministraGrupo()
        {
            return $this->AdministraGrupo;
        }

        public function SetAtribuiAtividade($AtribuiAtividade)
        {
            $this->AtribuiAtividade = $AtribuiAtividade;
        }
        public function GetAtribuiAtividade()
        {
            return $this->AtribuiAtividade;
        }

        public function SetPodeCompartilhar($PodeCompartilhar)
        {
            $this->PodeCompartilhar = $PodeCompartilhar;
        }
        public function GetPodeCompartilhar()
        {
            return $this->PodeCompartilhar;
        }
    }
?>