<?php

    class GrupoAtividade
    {
        private $IdGrupo;
        private $Grupo;
        private $Ativo;
        private $Descricao;
        private $IdUsuario;

        public function SetIdGrupo($IdGrupo)
        {
            $this->IdGrupo = $IdGrupo;
        }

        public function GetIdGrupo()
        {
            return $this->IdGrupo;
        }

        public function SetGrupo($Grupo)
        {
            $this->Grupo = $Grupo;
        }

        public function GetGrupo()
        {
            return $this->Grupo;
        }

        public function SetAtivo($Ativo)
        {
            $this->Ativo = $Ativo;
        }
        public function GetAtivo()
        {
            return $this->Ativo;
        }

        public function SetDescricao($Descricao)
        {
            $this->Descricao = $Descricao;
        }

        public function GetDescricao()
        {
            return $this->Descricao;
        }

        public function SetIdUsuario($IdUsuario)
        {
            $this->IdUsuario = $IdUsuario;
        }

        public function GetIdUsuario()
        {
            return $this->IdUsuario;
        }
    }
?>