<?php
    include_once 'modelBase.php';
    include_once 'entity/usuario.php';

    class ModelUsuario
    {
        public function Logar($email, $senha)
        {
            $con = AbrirConexao();

            $query = "Select * From Usuario Where Email = '".$email. "' AND Senha ='".$senha."'; ";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                $resultado = mysqli_fetch_assoc($executa);
            }

            FecharConexao($con);
        
            return $resultado;
        }

        public function Inserir(Usuario $Usuario)
        {
            $nome = $Usuario->GetNomeUsuario();
            $senha = $Usuario->GetSenha();
            $email = $Usuario->GetEmail();
            $ativo = $Usuario->GetAtivo();

            $con = AbrirConexao();

            $query = "INSERT INTO Usuario (NomeUsuario, Senha, Email, Ativo) VALUES ('$nome', '$senha','$email','$ativo');";

            $executa = mysqli_query($con, $query);

            $numLinha = mysqli_affected_rows($con);

            FecharConexao($con);
        
            return $numLinha;
        }

        public function EncontrarEmail($email)
        {
            $con = AbrirConexao();

            $query = "Select * From Usuario Where Email = '".$email. "'; ";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                $resultado = mysqli_fetch_assoc($executa);

                if(is_array($resultado))
                {
                    $retorno = false;
                }
                else
                {
                    $retorno = true;
                }
            }

            FecharConexao($con);
        
            return $retorno;
        }
        public function BuscarUsuarioPorEmail($email)
        {
            $con = AbrirConexao();

            $query = "Select * From Usuario Where Email = '".$email."' AND Ativo = 1;";
 
            $executa = mysqli_query($con, $query);

            while($linha = mysqli_fetch_assoc($executa))
            {
                $resultado[] = $linha; 
                
            }
        

            FecharConexao($con);

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $usuario = new Usuario();
                    $usuario->SetIdUsuario($row['IdUsuario']);
                    $usuario->SetNomeUsuario($row['NomeUsuario']);
                    $usuario->SetEmail($row['Email']);
    
                    $listaRetorno[] = $usuario;
                }
            }
            else
            {
                $listaRetorno = '';
            }
        
            return $listaRetorno;
        }
    }
    
?>