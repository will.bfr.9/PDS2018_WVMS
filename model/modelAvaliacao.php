<?php
    include_once 'modelBase.php';
    include_once 'entity/avaliacaoAtividade.php';

    class ModelAvaliacao
    {
        public function RetornaAvaliacao()
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT * FROM avaliacaoatividade WHERE Ativo = 1;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                }
            }

            FecharConexao($con);

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $avaliacao = new AvaliacaoAtividade();
                    $avaliacao->SetIdAvaliacao($row['IdAvaliacao']);
                    $avaliacao->SetAvaliacao($row['Avaliacao']);
                    $avaliacao->SetAtivo($row['Ativo']);
    
                    $listaRetorno[] = $avaliacao;
                }
            }
        
            return $listaRetorno;
        }

        public function AtividadesTotal($IdUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdAtividade) AS 'qtd' FROM atividade WHERE IdUsuarioAtribuido = $IdUsuario;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }

        public function AtividadesFeitas($IdUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdAtividade) AS 'qtd' FROM atividade WHERE IdUsuarioAtribuido = $IdUsuario AND DataEntrega IS NOT NULL;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }

        public function AtividadesAvaliadas($IdUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdAtividade) AS 'qtd' FROM atividade WHERE IdUsuarioAtribuido = $IdUsuario AND DataEntrega IS NOT NULL AND IdAvaliacao IS NOT NULL;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }

        public function AtividadesPorAvaliacao($IdUsuario, $IdAvaliacao)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdAtividade) AS 'qtd' FROM atividade WHERE IdUsuarioAtribuido = $IdUsuario AND DataEntrega IS NOT NULL AND IdAvaliacao = $IdAvaliacao;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }
    }
    
?>