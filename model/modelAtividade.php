<?php
    include_once 'modelBase.php';
    include_once 'entity/Atividade.php';

    class ModelAtividade
    {
        public function AtividadeRapida($idUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT IdGrupo FROM GrupoAtividade WHERE IdUsuario = $idUsuario AND Grupo = 'Minhas Atividades';";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['IdGrupo'];
        }
        public function Inserir(Atividade $Atividade)
        {
            $atividade = $Atividade->GetAtividade();
            $ativo = $Atividade->GetAtivo();
            $dataCriacao = $Atividade->GetDataCriacao();
            $descricao = $Atividade->GetDescricao();
            $idUsuarioAtribuido = $Atividade->GetIdUsuarioAtribuido();
            $idGrupo = $Atividade->GetIdGrupo();
            $idCriador = $Atividade->GetIdCriador();
            $idPrioridade = $Atividade->GetIdPrioridade();
            $idStatus = $Atividade->GetIdStatus();

            $con = AbrirConexao();
                
             
            $query = "INSERT INTO Atividade (Atividade, Ativo, DataCriacao, Descricao, 
                    IdUsuarioAtribuido, IdGrupo, IdCriador, IdPrioridade, 
                    IdStatus) VALUES ('$atividade', '$ativo', '$dataCriacao', '$descricao',
                    '$idUsuarioAtribuido','$idGrupo','$idCriador','$idPrioridade','$idStatus');";

            $executa = mysqli_query($con, $query);

            $numLinha = mysqli_affected_rows($con);

            FecharConexao($con);
        
            return $numLinha;
        }

        public function RetornaQtdAtividadesAbertas($idUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdAtividade) AS 'qtd' FROM Atividade WHERE IdUsuarioAtribuido = $idUsuario AND DataEntrega IS NULL;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }
        public function RetornaAtividadesAtribuidasUsuario($idUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT statusAtividade.NomeStatus, prioridadeAtividade.Prioridade, 
                      grupoatividade.Grupo, C.Email AS EmailCriador
                      , C.NomeUsuario AS NomeCriador, A.Email AS EmailAtribuido
                      , A.NomeUsuario AS NomeAtribuido,  
                      atividade.* FROM atividade INNER JOIN 
                      usuario A ON A.IdUsuario = atividade.IdUsuarioAtribuido INNER JOIN
                      usuario C ON C.IdUsuario = atividade.IdCriador INNER JOIN
                      grupoatividade ON grupoatividade.IdGrupo = atividade.IdGrupo INNER JOIN 
                      statusAtividade ON statusAtividade.IdStatus = atividade.IdStatus INNER JOIN 
                      prioridadeAtividade ON prioridadeAtividade.IdPrioridade = atividade.IdPrioridade WHERE 
                      atividade.IdUsuarioAtribuido = $idUsuario AND DataEntrega IS NULL;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                }
            }

            FecharConexao($con);

            $listaRetorno = null;

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $atividade = new Atividade();
                    $atividade->SetIdAtividade($row['IdAtividade']);
                    $atividade->SetIdGrupo($row['IdGrupo']);
                    $atividade->SetIdCriador($row['IdCriador']);
                    $atividade->SetIdUsuarioAtribuido($row['IdUsuarioAtribuido']);
                    $atividade->SetIdStatus($row['IdStatus']);
                    $atividade->SetIdPrioridade($row['IdPrioridade']);
                    $atividade->SetAtividade($row['Atividade']);
                    $atividade->SetDataCriacao($row['DataCriacao']);
                    $atividade->SetDescricao($row['Descricao']);

                    $atividade->SetNomeStatus($row['NomeStatus']);
                    $atividade->SetNomePrioridade($row['Prioridade']);
                    $atividade->SetNomeCriador($row['NomeCriador']);
                    $atividade->SetEmailCriador($row['EmailCriador']);
                    $atividade->SetNomeAtribuido($row['NomeAtribuido']);
                    $atividade->SetEmailAtribuido($row['EmailAtribuido']);
                    $atividade->SetNomeGrupo($row['Grupo']);
                    $listaRetorno[] = $atividade;
                }
            }
        
            return $listaRetorno;
        }
        public function RetornaAtividadesCriadasUsuario($idUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT statusAtividade.NomeStatus, prioridadeAtividade.Prioridade, 
                      grupoatividade.Grupo, C.Email AS EmailCriador
                      , C.NomeUsuario AS NomeCriador, A.Email AS EmailAtribuido
                      , A.NomeUsuario AS NomeAtribuido,  
                      atividade.* FROM atividade INNER JOIN 
                      usuario A ON A.IdUsuario = atividade.IdUsuarioAtribuido INNER JOIN
                      usuario C ON C.IdUsuario = atividade.IdCriador INNER JOIN
                      grupoatividade ON grupoatividade.IdGrupo = atividade.IdGrupo INNER JOIN 
                      statusAtividade ON statusAtividade.IdStatus = atividade.IdStatus INNER JOIN 
                      prioridadeAtividade ON prioridadeAtividade.IdPrioridade = atividade.IdPrioridade WHERE 
                      atividade.IdCriador = $idUsuario AND DataEntrega IS NULL;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                }
            }

            FecharConexao($con);

            $listaRetorno = null;

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $atividade = new Atividade();
                    $atividade->SetIdAtividade($row['IdAtividade']);
                    $atividade->SetIdGrupo($row['IdGrupo']);
                    $atividade->SetIdCriador($row['IdCriador']);
                    $atividade->SetIdUsuarioAtribuido($row['IdUsuarioAtribuido']);
                    $atividade->SetIdStatus($row['IdStatus']);
                    $atividade->SetIdPrioridade($row['IdPrioridade']);
                    $atividade->SetAtividade($row['Atividade']);
                    $atividade->SetDataCriacao($row['DataCriacao']);
                    $atividade->SetDescricao($row['Descricao']);

                    $atividade->SetNomeStatus($row['NomeStatus']);
                    $atividade->SetNomePrioridade($row['Prioridade']);
                    $atividade->SetNomeCriador($row['NomeCriador']);
                    $atividade->SetEmailCriador($row['EmailCriador']);
                    $atividade->SetNomeAtribuido($row['NomeAtribuido']);
                    $atividade->SetEmailAtribuido($row['EmailAtribuido']);
                    $atividade->SetNomeGrupo($row['Grupo']);
                    $listaRetorno[] = $atividade;
                }
            }
        
            return $listaRetorno;
        }
        public function BuscaAtividadePorId($idAtividade)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT statusAtividade.NomeStatus, prioridadeAtividade.Prioridade, 
                      grupoatividade.Grupo, C.Email AS EmailCriador
                      , C.NomeUsuario AS NomeCriador, A.Email AS EmailAtribuido
                      , A.NomeUsuario AS NomeAtribuido,  
                      atividade.* FROM atividade INNER JOIN 
                      usuario A ON A.IdUsuario = atividade.IdUsuarioAtribuido INNER JOIN
                      usuario C ON C.IdUsuario = atividade.IdCriador INNER JOIN
                      grupoatividade ON grupoatividade.IdGrupo = atividade.IdGrupo INNER JOIN 
                      statusAtividade ON statusAtividade.IdStatus = atividade.IdStatus INNER JOIN 
                      prioridadeAtividade ON prioridadeAtividade.IdPrioridade = atividade.IdPrioridade WHERE 
                      atividade.IdAtividade = $idAtividade;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                }
            }

            FecharConexao($con);

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $atividade = new Atividade();
                    $atividade->SetIdAtividade($row['IdAtividade']);
                    $atividade->SetIdGrupo($row['IdGrupo']);
                    $atividade->SetIdCriador($row['IdCriador']);
                    $atividade->SetIdUsuarioAtribuido($row['IdUsuarioAtribuido']);
                    $atividade->SetIdStatus($row['IdStatus']);
                    $atividade->SetIdPrioridade($row['IdPrioridade']);
                    $atividade->SetAtividade($row['Atividade']);
                    $atividade->SetDataCriacao($row['DataCriacao']);
                    $atividade->SetDescricao($row['Descricao']);
                    $atividade->SetIdAvaliacao($row['IdAvaliacao']);

                    $atividade->SetNomeStatus($row['NomeStatus']);
                    $atividade->SetNomePrioridade($row['Prioridade']);
                    $atividade->SetNomeCriador($row['NomeCriador']);
                    $atividade->SetEmailCriador($row['EmailCriador']);
                    $atividade->SetNomeAtribuido($row['NomeAtribuido']);
                    $atividade->SetEmailAtribuido($row['EmailAtribuido']);
                    $atividade->SetNomeGrupo($row['Grupo']);
                    $listaRetorno[] = $atividade;
                }
            }
        
            return $listaRetorno;
        }

        public function VerificaUsuarioAtribuidoAtividade($idAtividade, $idUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdAtividade) AS 'qtd' FROM Atividade WHERE IdUsuarioAtribuido = $idUsuario AND IdAtividade = $idAtividade AND DataEntrega IS NULL;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }

        public function VerificaUsuarioCriadorAtividade($idAtividade, $idUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdAtividade) AS 'qtd' FROM Atividade WHERE IdCriador = $idUsuario AND IdAtividade = $idAtividade AND DataEntrega IS NOT NULL;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }

        public function Atualizar(Atividade $Atividade)
        {
            $idAtividade = $Atividade->GetIdAtividade();
            $atividade = $Atividade->GetAtividade();
            $ativo = $Atividade->GetAtivo();
            $dataCriacao = $Atividade->GetDataCriacao();
            $descricao = $Atividade->GetDescricao();
            $idUsuarioAtribuido = $Atividade->GetIdUsuarioAtribuido();
            $idGrupo = $Atividade->GetIdGrupo();
            $idCriador = $Atividade->GetIdCriador();
            $idPrioridade = $Atividade->GetIdPrioridade();
            $idStatus = $Atividade->GetIdStatus();
            $dataEntrega = $Atividade->GetDataEntrega();

            $con = AbrirConexao();
                
            if($dataEntrega==null)
            {
                $query = "UPDATE Atividade SET Atividade = '$atividade', Ativo = $ativo, 
                    Descricao = '$descricao', 
                    IdUsuarioAtribuido = $idUsuarioAtribuido, IdGrupo = $idGrupo, 
                    IdCriador = $idCriador, IdPrioridade = $idPrioridade, 
                    IdStatus = $idStatus WHERE IdAtividade = $idAtividade;";
            } 
            else
            {
                $query = "UPDATE Atividade SET Atividade = '$atividade', Ativo = $ativo, 
                    Descricao = '$descricao', 
                    IdUsuarioAtribuido = $idUsuarioAtribuido, IdGrupo = $idGrupo, 
                    IdCriador = $idCriador, IdPrioridade = $idPrioridade, 
                    IdStatus = $idStatus, DataEntrega = '$dataEntrega' WHERE IdAtividade = $idAtividade;";
            }

            $executa = mysqli_query($con, $query);

            $numLinha = mysqli_affected_rows($con);

            FecharConexao($con);
        
            return $numLinha;
        }
        
        public function RetornaQtdAtividadesParaAvaliar($idUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT COUNT(IdAtividade) AS 'qtd' FROM Atividade WHERE IdCriador = $idUsuario AND DataEntrega IS NOT NULL AND IdAvaliacao IS NULL;";

            $executa = mysqli_query($con, $query);

            $resultado = mysqli_fetch_assoc($executa);

            FecharConexao($con);
        
            return $resultado['qtd'];
        }
        public function RetornaAtividadesCriadasParaAvaliar($idUsuario)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT statusAtividade.NomeStatus, prioridadeAtividade.Prioridade, 
                      grupoatividade.Grupo, C.Email AS EmailCriador
                      , C.NomeUsuario AS NomeCriador, A.Email AS EmailAtribuido
                      , A.NomeUsuario AS NomeAtribuido,  
                      atividade.* FROM atividade INNER JOIN 
                      usuario A ON A.IdUsuario = atividade.IdUsuarioAtribuido INNER JOIN
                      usuario C ON C.IdUsuario = atividade.IdCriador INNER JOIN
                      grupoatividade ON grupoatividade.IdGrupo = atividade.IdGrupo INNER JOIN 
                      statusAtividade ON statusAtividade.IdStatus = atividade.IdStatus INNER JOIN 
                      prioridadeAtividade ON prioridadeAtividade.IdPrioridade = atividade.IdPrioridade WHERE 
                      atividade.IdCriador = $idUsuario AND DataEntrega IS NOT NULL AND IdAvaliacao IS NULL;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                }
            }

            FecharConexao($con);

            $listaRetorno = null;

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $atividade = new Atividade();
                    $atividade->SetIdAtividade($row['IdAtividade']);
                    $atividade->SetIdGrupo($row['IdGrupo']);
                    $atividade->SetIdCriador($row['IdCriador']);
                    $atividade->SetIdUsuarioAtribuido($row['IdUsuarioAtribuido']);
                    $atividade->SetIdStatus($row['IdStatus']);
                    $atividade->SetIdPrioridade($row['IdPrioridade']);
                    $atividade->SetAtividade($row['Atividade']);
                    $atividade->SetDataCriacao($row['DataCriacao']);
                    $atividade->SetDescricao($row['Descricao']);

                    $atividade->SetNomeStatus($row['NomeStatus']);
                    $atividade->SetNomePrioridade($row['Prioridade']);
                    $atividade->SetNomeCriador($row['NomeCriador']);
                    $atividade->SetEmailCriador($row['EmailCriador']);
                    $atividade->SetNomeAtribuido($row['NomeAtribuido']);
                    $atividade->SetEmailAtribuido($row['EmailAtribuido']);
                    $atividade->SetNomeGrupo($row['Grupo']);
                    $listaRetorno[] = $atividade;
                }
            }
        
            return $listaRetorno;
        }
        public function Avaliar(Atividade $Atividade)
        {
            $idAtividade = $Atividade->GetIdAtividade();
            $idAvaliacao = $Atividade->GetIdAvaliacao();

            $con = AbrirConexao();
                
            $query = "UPDATE Atividade SET IdAvaliacao = '$idAvaliacao'
            WHERE IdAtividade = $idAtividade;";
            

            $executa = mysqli_query($con, $query);

            $numLinha = mysqli_affected_rows($con);

            FecharConexao($con);
        
            return $numLinha;
        }
    }
    
?>