<?php
    function AbrirConexao()
    {
        $servidor = 'localhost';
        $usuario = 'root';
        $senha = '';
        $banco = 'pds_2018';

        $con = new mysqli($servidor, $usuario, $senha, $banco);
        $con->set_charset("utf8");
        if($con->connect_error)
        {
            die("Erro de Conexão: " .$con->connect_error());
        }
        return $con;
    }      
    
    function FecharConexao($con)
    {
        $con->close();
    }
?>