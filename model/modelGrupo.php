<?php
    include_once 'modelBase.php';
    include_once 'entity/grupoAtividade.php';

    class ModelGrupo
    {
        public function Inserir(GrupoAtividade $Grupo)
        {
            $nomeGrupo = $Grupo->GetGrupo();
            $idUsuario = $Grupo->GetIdUsuario();
            $descricao = $Grupo->GetDescricao();
            $ativo = $Grupo->GetAtivo();

            $con = AbrirConexao();
                
            
            $query = "INSERT INTO GrupoAtividade (Grupo, IdUsuario, Descricao, Ativo) VALUES ('$nomeGrupo', '$idUsuario', '$descricao', '$ativo');";

            $executa = mysqli_query($con, $query);

            $numLinha = mysqli_affected_rows($con);

            FecharConexao($con);
        
            return $numLinha;
        }

        public function BuscarGrupoId($idGrupo)
        {
            $con = AbrirConexao();
                
            
            $query = "SELECT * FROM grupoAtividade WHERE IdGrupo = $idGrupo;";

            $executa = mysqli_query($con, $query);

            if(empty($executa))
            {
                $resultado = "";
            }
            else
            {
                while($linha = mysqli_fetch_assoc($executa))
                {
                    $resultado[] = $linha; 
                    
                }
            }

            FecharConexao($con);

            if(isset($resultado))
            {
                $listaRetorno = array();
                foreach($resultado as $row)
                {
                    $grupo = new GrupoAtividade();
                    $grupo->SetIdGrupo($row['IdGrupo']);
                    $grupo->SetGrupo($row['Grupo']);
                    $grupo->SetDescricao($row['Descricao']);
                    $grupo->SetIdUsuario($row['IdUsuario']);
                    $grupo->SetAtivo($row['Ativo']);
    
                    $listaRetorno[] = $grupo;
                }
            }
        
            return $listaRetorno;
        }
    }
    
?>