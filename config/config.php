<?php
    define('BASE_URL', "http://" .$_SERVER['HTTP_HOST'].'/PDS/');

    define('IMG_URL', BASE_URL.'recursos/imagens/');

    define('CSS_URL', BASE_URL.'recursos/estilos/');

    define('JS_URL', BASE_URL.'recursos/js/');

    session_start();

?>